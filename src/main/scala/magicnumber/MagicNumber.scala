package magicnumber

object MagicNumber extends App {
  def isMagicNumber(number: Array[Int]) = {
    val r = for (i <- number.indices) yield {
      val index = number(i)
      val nIndex = (i + index) % number.length
      if (nIndex == i) {
        -1
      } else {
        number(nIndex)
      }
    }
    number.intersect(r).sameElements(number)
  }

  def run(start: Int, end: Int): Seq[String] = {
    val l: Seq[String] = (start until end)
      .map(_.toString.toCharArray.map(_.toString.toInt))
      .filter(m => m.distinct.length == m.length)
      .filter(isMagicNumber)
      .map(m => m.mkString(""))
    l
  }

  println(run(100, 1000).mkString(" "))
}
