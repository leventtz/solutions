package number2word

object Solution1Recursive extends App{
  val digitMap=Map('0' ->Seq("0"),'1'->Seq("1"),'2' -> Seq("a","b","c"),'3'->Seq("d","e","f"),'4'->Seq("g","h","i"),'5' -> Seq("j","k","l"),'6' ->Seq("m","n","o"),'7'->Seq("p","q","r","s"),'8'->Seq("t","u","v"),'9'->Seq("w","x","y","z"));
  val m="4155230".toList.map(a => digitMap(a))

  println(convertPermutation("4155230").mkString(","))

  def convertPermutation(str: Seq[Char]): Seq[String] = {
    str match {
      case Seq()  => Seq.empty
      case Seq(c) => digitMap(c)
      case Seq(head, tail @ _*) =>
        val t = convertPermutation(tail)
        val m = digitMap(head)
        m.flatMap(pre => t.map(pre + _))
    }
  }
}
