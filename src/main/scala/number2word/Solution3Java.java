package number2word;

import java.util.*;

public class Solution3Java {
    public static final Map<Character, List<String>> digitMap= new HashMap<>();
    static {
        digitMap.put('0', Arrays.asList("0"));
        digitMap.put('1', Arrays.asList("1"));
        digitMap.put('2', Arrays.asList("a", "b", "c"));
        digitMap.put('3', Arrays.asList("d", "e", "f"));
        digitMap.put('4', Arrays.asList("g", "h", "i"));
        digitMap.put('5', Arrays.asList("j", "k", "l"));
        digitMap.put('6', Arrays.asList("m", "n", "o"));
        digitMap.put('7', Arrays.asList("p", "q", "r", "s"));
        digitMap.put('8', Arrays.asList("t", "u", "v"));
        digitMap.put('9', Arrays.asList("w", "x", "y", "z"));
    }
    public static List<String> combine(List<String> list1,List<String> list2) {
        List<String> listCombine=new ArrayList<>();
        for (String s : list1) {
            for (String value : list2) {
                listCombine.add(s + value);
            }
        }
        return listCombine;
    }

    private static String convertPermutation(String number) {
        List<String> accumulator=digitMap.get(number.charAt(0));
        for(int i=1;i<number.length();i++) {
            accumulator=combine(accumulator,digitMap.get(number.charAt(i)));
        }
        StringBuilder builder=new StringBuilder();
        for (String s : accumulator) {
            builder.append(s).append(" ");
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        System.out.println(convertPermutation("4155230"));
    }
}
